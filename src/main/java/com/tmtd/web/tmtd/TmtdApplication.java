package com.tmtd.web.tmtd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmtdApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmtdApplication.class, args);
	}

}
