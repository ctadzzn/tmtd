package com.tmtd.web.tmtd.domain;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user")
    private Long id;

    // @Column(name = "accessToken", length = 2048, nullable = false)
    // private String accessToken;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "imageUrl", length = 2048)
    private String imageUrl;

    @Column(name = "createdAt", nullable = false)
    private Date createdAt = new Date(); 

    @Column(name = "status")
    private String status;

}
