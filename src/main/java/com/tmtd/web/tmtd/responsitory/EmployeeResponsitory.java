package com.tmtd.web.tmtd.responsitory;

import java.util.Date;
import java.util.List;

import com.tmtd.web.tmtd.domain.Employee;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unused")
@Repository
public interface EmployeeResponsitory extends JpaRepository<Employee, Long> {
    // @Query(nativeQuery = true, value = "select * from Employee" )
    // public Employee getEmployee(@Param("date") Date date);

    // public List<Employee> findAllByOrderByIdAsc();
}
