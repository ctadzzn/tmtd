package com.tmtd.web.tmtd.service;

import java.util.List;

import com.tmtd.web.tmtd.domain.Employee;
import com.tmtd.web.tmtd.responsitory.EmployeeResponsitory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Controller
public class NewAPI {

        @Autowired
        private EmployeeResponsitory employeeRes;

        @GetMapping("/employees")
        public String listAll(Model model) {
                List<Employee> listEmployees = employeeRes.findAll();
                model.addAttribute("listEmployees", listEmployees);

                return "listEmployees";
        }
}